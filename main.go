package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"golang.org/x/crypto/bcrypt"
)

var db *gorm.DB
var err error

//User is a representation of a user
type Users struct {
	gorm.Model
	Id int `json:"id"`
	jwt.StandardClaims
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required,gte=5"`
}

//for JWT Auth
type M map[string]interface{}

var APPLICATION_NAME = "JWT for Test Yulius"
var LOGIN_EXPIRATION_DURATION = time.Duration(1) * time.Hour
var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_SIGNATURE_KEY = []byte("yulius")

type Posts struct {
	gorm.Model
	//Id           uint   `gorm:"primaryKey,autoIncrement"`
	Id_User      uint   `json:"id_user" validate:"required"`
	Title        string `json:"title" validate:"required"`
	Content      string `json:"content" validate:"required"`
	Has_Tag      Has_Tag
	Has_Categori Has_Categori
}

type Categories struct {
	gorm.Model
	Id       int    `json:"id"`
	Categori string `json:"categori" validate:"required"`
}

type Tags struct {
	gorm.Model
	Id  int    `json:"id"`
	Tag string `json:"tag" validate:"required"`
}

type Has_Tag struct {
	gorm.Model
	Id      int  `json:"id"`
	ID_Post uint `json:"id_post" validate:"required"`
	ID_Tag  uint `json:"id_tag" validate:"required"`
}

type Has_Categori struct {
	gorm.Model
	Id          int  `json:"id"`
	ID_Post     uint `json:"id_post" validate:"required"`
	ID_Categori uint `json:"id_categori" validate:"required"`
}

func main() {
	db, err = gorm.Open("mysql", "root:@/project01?charset:utf8&parseTime=True")

	if err != nil {
		log.Println("Connection failed", err)
	} else {
		log.Println("Connection established")
	}

	//db.AutoMigrate(&Users{}, &Posts{}, &Tags{}, &Categories{})
	db.AutoMigrate(&Users{}, &Posts{}, &Tags{}, &Has_Tag{}, &Categories{}, &Has_Categori{})
	defer db.Close()
	handleRequests()

}

func handleRequests() {
	r := gin.Default()
	r.POST("/", index)
	r.GET("/get", MiddlewareJWTAuthorization(), getUser)
	r.POST("/login", login)
	r.POST("/createPost", createPost)
	r.POST("/createTag", createTag)
	r.POST("/createCategori", createCategori)

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}

func index(c *gin.Context) {

	var input Users

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	hashPass, err := hashPassword(input.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	input.Password = hashPass

	db.Create(&input)

	c.JSON(http.StatusOK, gin.H{"data": input.Username, "pass": hashPass})
}

func createCategori(c *gin.Context) {
	var input Categories
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db.FirstOrCreate(&input, input)

	c.JSON(http.StatusOK, gin.H{"Tag": input.Categori, "Id": input.ID})
}

func createTag(c *gin.Context) {
	var input Tags
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db.FirstOrCreate(&input, input)

	c.JSON(http.StatusOK, gin.H{"Tag": input.Tag, "Id": input.ID})
}

/* contoh request create post
{   "id_user" : 10,
"title" : "minuman bergizi",
"content" : "minumlah minuman yang bergizi",
"tags" : {"tag":"#coba"},
"categories" : {"categori":"minuman"}
}
*/
func createPost(c *gin.Context) {

	var input Posts

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	input.Has_Tag.ID_Post = input.ID
	input.Has_Categori.ID_Post = input.ID

	post := Posts{Id_User: input.Id_User,
		Title:        input.Title,
		Content:      input.Content,
		Has_Tag:      Has_Tag{ID_Tag: input.Has_Tag.ID, ID_Post: input.ID},
		Has_Categori: Has_Categori{ID_Categori: input.Has_Categori.ID_Categori, ID_Post: input.ID},
	}

	db.Create(&post)
	c.JSON(http.StatusOK, gin.H{"Title": input})
}

func getUser(c *gin.Context) {
	var input Users
	var usr Users
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	db.Find(&usr, "username = ?", input.Username)
	match := CheckPasswordHash(input.Password, usr.Password)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, gin.H{"data": input.Password, "pass": match})

}

func login(c *gin.Context) {
	var input Users
	var user Users

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ok, userInfo := authenticateUser(input.Username, input.Password)
	if !ok {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid username or password"})
		return
	}

	claims := Users{
		StandardClaims: jwt.StandardClaims{
			Issuer:    APPLICATION_NAME,
			ExpiresAt: time.Now().Add(LOGIN_EXPIRATION_DURATION).Unix(),
		},
		Username: userInfo.Username,
		Id:       userInfo.Id,
	}

	token := jwt.NewWithClaims(
		JWT_SIGNING_METHOD,
		claims,
	)

	signedToken, err := token.SignedString(JWT_SIGNATURE_KEY)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tokenString, _ := json.Marshal(M{"token": signedToken})
	db.Find(&user, "username = ?", userInfo.Username)
	db.Model(&user).Update(claims)
	c.JSON(http.StatusOK, gin.H{"token": tokenString})

}

func authenticateUser(username, password string) (bool, Users) {

	var M Users

	db.Where("username = ? ", username).Find(&M)

	match := CheckPasswordHash(password, M.Password)

	if !match {
		return false, M
	}

	return true, M

}

//hashing user password
func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

//to check every request that come into endpoint has a valid access token or not
func MiddlewareJWTAuthorization() gin.HandlerFunc {
	return (func(c *gin.Context) {

		authorizationHeader := c.Request.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid token"})
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)
		//masih eror disini (err)
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			http.Error(c.Writer, err.Error(), http.StatusBadRequest)
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		ctx := context.WithValue(context.Background(), "userInfo", claims)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
		//next.ServeHTTP(c.Writer, c.Request)
		// ...
	})
}
